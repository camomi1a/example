import React from 'react';
import { renderRoutes } from 'react-router-config';
import Preloader from '../components/Preloader';

function App(props) {
  return (
    <div className='App'>
      <Preloader/>
      {renderRoutes(props.route.routes)}
    </div>
  );
}

export default App;
