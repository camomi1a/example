import { get } from './http';
import queryString from 'query-string';

const baseUrl = 'https://somedomain.com/api/';

export function apiGetGoods(search) {
  let locationSearch = '';

  if (search) locationSearch = `?${typeof (search) === 'string' ? search : queryString.stringify(search)}`;

  return get({ url: `${baseUrl}goods/${locationSearch}` });
}
