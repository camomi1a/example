export const addProductToCart = 'ADD_PRODUCT_TO_CART';
export const removeProductFromCart = 'REMOVE_PRODUCT_FROM_CART';
export const deleteProductFromCartConst = 'DELETE_PRODUCT_FROM_CART';
export const clearCartConst = 'CLEAR_CART';
export const getCart = 'GET_CART';
export const postCart = 'POST_CART';
export const updateCart = 'UPDATE_CART';
