import React from 'react';
import { PropTypes } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import CatalogProduct from '../CatalogProduct';
import DeleteIcon from '@material-ui/icons/Delete';
import { useStyles } from './styles';
import { deleteProductFromCartAction, clearCartAction } from './actions';
import Typography from '@material-ui/core/Typography';

export default function Cart() {
  const cart = useSelector(state => state.cart);
  const dispatch = useDispatch();

  function deleteItem(item) {
    dispatch(deleteProductFromCartAction(item));
  }

  return (
    <div>
      {!!cart.products.length
        ? <>
          <Typography onClick={() => dispatch(clearCartAction())}> Удалить все</Typography>
          {(cart.products || []).map((item) => {
            return (
              <CatalogProduct componentClasses={useStyles} key={item.id} product={item} contentStyle={{ display: 'flex' }}>
                <DeleteIcon onClick={deleteItem.bind(this, item)}/>
              </CatalogProduct>
            );
          })}</>
        : <div>Корзина пуста</div>}
    </div>
  );
}

Cart.propTypes = {
  cart: PropTypes.object,
};
