import { getCart, postCart } from './constants';
import { FINISH_CONST } from '../../redux/constants';

const cartInitialState = localStorage.cart ? JSON.parse(localStorage.cart) : { timeStamp: Date.now(), products:[], counter: 0 };

export function getCartReducer(state = cartInitialState, action) {
  switch (action.type) {
    case `${postCart + FINISH_CONST}`: return { ...state, ...action.payload };
    case getCart: return { ...state };
    default: return  state;
  }
}
