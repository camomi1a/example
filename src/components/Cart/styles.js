import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  root: {
    margin: 8,
    maxWidth: 645,
    '&:hover':{
      boxShadow: '0 1px 3px 0 rgba(0,0,0,.85)',
    },
  },
  margin:{
    margin: 8,
  },
  img:{
    width: 240,
  },
});
