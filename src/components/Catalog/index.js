import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { mergeCatalogWithCart } from '../../helpers/mergeCatalogWithCart';
import Grid from '@material-ui/core/Grid';
import Filters from '../Filters';
import Hidden from '@material-ui/core/Hidden';
import CatalogProduct from '../CatalogProduct';
import { catalogProductStyles, useStyles } from './styles';

function Catalog({
  catalog,
}) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Hidden smDown>
          <Grid item mdUp md={2}>
            <Filters/>
          </Grid>
        </Hidden>
        <Grid container xs={12} md={10}>
          {catalog.map((product) => {
            return (
              <Grid key={product.id} item xs={12} sm={6} lg={4}>
                <CatalogProduct componentClasses={catalogProductStyles} product={product}/>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </div>
  );
}

Catalog.propTypes = {
  products: PropTypes.array,
  catalog: PropTypes.array,
  getCatalog: PropTypes.func,
};

export default connect((state) => {
  return {
    catalogProducts: state.catalog.products,
    cartProducts: state.cart.products,
    catalog: mergeCatalogWithCart(state.cart.products, state.catalog.products),
  };
})(Catalog);

