import { getCatalogConst } from '../Catalog/constants';
import asyncAction from '../../redux/actions-factory';
import { apiGetCatalog } from './promises';
// import { apiGetGoods } from '../../api/api';

export const catalogMiddleware = store => next => action => {
  switch (action.type) {
    case getCatalogConst : store.dispatch(asyncAction({
      ...action,
      // actionPromise: apiGetGoods(),
      actionPromise: apiGetCatalog,
    })); break;
  }

  return next(action);
};

