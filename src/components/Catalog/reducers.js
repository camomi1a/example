import { getCatalogConst } from './constants';
import { postCart } from '../Cart/constants';
import { FINISH_CONST } from '../../redux/constants';

const catalogInitialState = { products:[] };

export function getCatalogReducer(state = catalogInitialState, action) {
  switch (action.type) {
    case `${getCatalogConst + FINISH_CONST}`: return { ...state, products: action.payload };
    default: return  state;
  }
}
