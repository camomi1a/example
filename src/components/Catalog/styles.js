import { makeStyles } from '@material-ui/core/styles';

export const catalogProductStyles = makeStyles({
  root: {
    margin: 8,
    maxWidth: 345,
    '&:hover':{
      boxShadow: '0 1px 3px 0 rgba(0,0,0,.85)',
    },
  },
  margin:{
    margin: 8,
  },
  img:{
    height: 240,
  },
});

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));
