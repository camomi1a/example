import React from 'react';
import { PropTypes } from 'prop-types';
import { useDispatch } from 'react-redux';
import { addProductToCartAction, removeProductFromCartAction } from '../Cart/actions';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Typography from '@material-ui/core/Typography';


export default function CatalogProduct({
  product,
  contentStyle,
  componentClasses = {},
  children = null,
}) {
  const dispatch = useDispatch();
  const classes = componentClasses();


  function  addItem(item) {
    dispatch(addProductToCartAction(item));
  }
  function removeItem(item) {
    dispatch(removeProductFromCartAction(item));
  }

  return (
    <Paper style={contentStyle} className={classes.root}>
      <CardMedia
        className={classes.img}
        component='img'
        alt={product.title}
        src={product.image}
        title={product.title}
      />
      <CardContent>
        <Typography gutterBottom variant='h5' component='h2'>
          {product.title}
        </Typography>
        <Typography variant='body2' color='textSecondary' component='p'>
          Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
          across all continents except Antarctica
        </Typography>
      </CardContent>
      <Grid
        container
        direction='row'
        justify='space-between'
        alignItems='center'
      >
        <ButtonGroup className={classes.margin} size='small' aria-label='small outlined button group'>
          <Button onClick={removeItem.bind(this, product)} disabled={!product.quantity}>-</Button>
          <Button>{product.quantity}</Button>
          <Button onClick={addItem.bind(this, product)}>+</Button>
        </ButtonGroup>
        <Typography className={classes.margin} gutterBottom variant='h5' component='h2'>
          ${product.price}
        </Typography>
      </Grid>
      {children}
    </Paper>
  );
}

CatalogProduct.propTypes = {
  product: PropTypes.object,
  contentStyle: PropTypes.object,
  componentClasses: PropTypes.object,
};
