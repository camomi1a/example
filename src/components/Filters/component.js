import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControlLabel } from '@material-ui/core';

export default function Filter({
  name = '',
  title = '',
  filters,
  initialValues = [],
  onChange,
}) {

  useEffect(() => {
    filters.forEach((filter) => {
      filter.checked = !!~initialValues.indexOf(filter.value);
    });
  }, []);

  function handleChange(filter, context, value) {
    filter.checked = value;
    const selectedFilters = [];

    filters.map(fl => {
      if (fl.checked) selectedFilters.push(fl.value);
    });

    onChange({ [name]: selectedFilters });
  }


  return (
    <>
      {!!title.length && <h1>{title}</h1>}
      {!!filters.length && filters.map((filter) => (
        <FormControlLabel
          control={
            <Checkbox
              checked={filter.checked}
              onChange={handleChange.bind(this, filter)}
              key={filter.value}
            />}
          label= {filter.title}
        />
      ))}
    </>
  );
}

Filter.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  filters: PropTypes.array,
  initialValues: PropTypes.array,
  onChange: PropTypes.func,
};
