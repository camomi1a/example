import { applyFiltersConst, getFiltersConst } from './constants';
import asyncAction from '../../redux/actions-factory';
import { apiGetFilters } from './promises';
import { replace } from 'connected-react-router';
import queryString from 'query-string';
import { getCatalogAction } from '../Catalog/actions';
import { applyFiltersAction } from './actions';
import { postCart } from '../Cart/constants';
import { FINISH_CONST } from '../../redux/constants';

export const filtersMiddleware = store => next => action => { // eslint-disable-line

  switch (action.type) {
    case getFiltersConst : store.dispatch(asyncAction({ ...action, actionPromise: apiGetFilters })); break;
    case `${getFiltersConst + FINISH_CONST}` : store.dispatch(applyFiltersAction()); break;
    case applyFiltersConst : {
      const location = store.getState().router.location;
      const search = { ...queryString.parse(location.search, { arrayFormat: 'comma' }), ...action.payload };
      const urlSearch = queryString.stringify(search, { arrayFormat: 'comma' });

      store.dispatch(replace(`${location.pathname}?${urlSearch}`));
      store.dispatch(getCatalogAction(!!urlSearch.length ? search : {}));
    } break;
  }

  return next(action);
};

