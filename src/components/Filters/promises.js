import { filters } from './constants';

export const apiGetFilters = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve({ timeStamp: Date.now(), filters });
  }, 300);
});
