import { getFiltersConst } from './constants';
import { postCart } from '../Cart/constants';
import { FINISH_CONST } from '../../redux/constants';

const filtersInitialState = { filters:[], timeStamp: Date.now() };

export function getFiltersReducer(state = filtersInitialState, action) {
  switch (action.type) {
    case `${getFiltersConst + FINISH_CONST}`: return { ...state, ...action.payload };
    default: return  state;
  }
}
