import React from 'react';
import { PropTypes } from 'prop-types';
import { useStyles } from './styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { useSelector } from 'react-redux';

export default function Header() {
  const cartCounter = useSelector(state => state.cart.counter);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Container fixed>
          <Toolbar>
            <Link to={'/catalog/'} className={classes.headerItems}>
              <MenuItem>
                Каталог
              </MenuItem>
            </Link>
            <Link to={'/coments'} className={classes.headerItems}>
              <MenuItem>
                Комментарии
              </MenuItem>
            </Link>
            <Typography variant='h6' className={classes.title} />
            <Link to={'/cart/'}>
              <IconButton className={classes.headerItems}>
                <Badge badgeContent={cartCounter} color='secondary'>
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </Link>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}

Header.propTypes = {
  cartCounter: PropTypes.number,
};
