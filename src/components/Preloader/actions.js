import { preloaderConst } from './constants';

export function setPreloaderAction(val) {
  return { type: preloaderConst, payload: val  };
}
