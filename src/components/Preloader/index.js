import React from 'react';
import { useSelector } from 'react-redux';
import { useStyles } from './styles';
import clsx from 'clsx';

export default function Preloader() {
  const preloaderState = useSelector(state => state.preloader.state);
  const classes = useStyles();

  return (<>
    {/* <div hidden={!preloaderState}>loading....</div>*/}
    <div className={clsx(classes.bounce, { [classes.hidden]: !preloaderState })}>
      <div className={classes.bg}/>
      <div className={`${classes.bounce1} ${classes.child}`}/>
      <div className={`${classes.bounce2} ${classes.child}`}/>
      <div className={`${classes.bounce3} ${classes.child}`}/>
    </div>
  </>);
}

Preloader.propTypes = {};
