// import { preloaderConst } from './constants';
import { setPreloaderAction } from './actions';
import { postCart } from '../Cart/constants';
import { FINISH_CONST, ERROR_CONST, START_CONST } from '../../redux/constants';

export const preloaderMiddleware = store => next => action => {// eslint-disable-line

  if (!!~action.type.indexOf(FINISH_CONST)) store.dispatch(setPreloaderAction(false));
  if (!!~action.type.indexOf(ERROR_CONST)) store.dispatch(setPreloaderAction(false));
  if (!!~action.type.indexOf(START_CONST)) store.dispatch(setPreloaderAction(true));

  return next(action);
};
