import { preloaderConst } from './constants';

const preloaderInitialState = { state : false };

export function preloaderReducer(state = preloaderInitialState, action) {
  switch (action.type) {
    case preloaderConst: return { ...state, state: action.payload };
    default: return  state;
  }
}
