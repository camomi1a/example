import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  bounce: {
    width: '100%',
    height: '100%',
    margin: 'auto',
    textAlign: 'center',
    position: 'fixed',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bg:{
    backgroundColor: 'white',
    opacity: 0.8,
    width: '100%',
    height: '100%',
    position: 'absolute',
    zIndex: 2,
  },
  child: {
    width: 20,
    height: 20,
    backgroundColor: theme.palette.primary.main,
    borderRadius: '100%',
    display: 'inline-block',
    animation: '$bounce 1.4s ease-in-out 0s infinite both',
    zIndex: 3,
  },
  bounce1: {
    animationDelay: '-0.32s',
  },
  bounce2: {
    animationDelay: '-0.16s',
  },
  bounce3: {
    animationDelay: '0s',
  },
  '@keyframes bounce': {
    '0%': {
      transform: 'scale(0)',
    },
    '80%': {
      transform: 'scale(0)',
    },
    '100%': {
      transform: 'scale(0)',
    },
    '40%': {
      transform: 'scale(1)',
    },
  },
  hidden: {
    display: 'none!important',
  },

}));
