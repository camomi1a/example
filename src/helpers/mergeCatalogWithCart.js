
export function mergeCatalogWithCart(cart, catalog) {
  catalog.forEach((product) => {
    cart.forEach((cartProduct) => {
      if (cartProduct.id === product.id) product.quantity = cartProduct.quantity;
    });
  });
  return catalog;
}
