import toConstantCase from 'to-constant-case';
import { ERROR_CONST, FINISH_CONST, START_CONST } from './constants';

export default function asyncAction(params) {
  const type = toConstantCase(params.type);

  function actionStarted() {
    // console.log(type, START_CONST);
    return { type: `${type + START_CONST}` };
  }

  function actionFinished(successData) {
    // console.log(type, FINISH_CONST, successData);
    return { type: `${type + FINISH_CONST}`, payload: successData };
  }

  function actionError(errors) {
    // console.log(type, ERROR_CONST);
    return { type: `${type + ERROR_CONST}`, payload: errors };
  }

  return (dispatch) => {
    dispatch(actionStarted());

    return params.actionPromise(params.payload)
      .then(data => dispatch(actionFinished(data)))
      .catch(errs  => dispatch(actionError(errs)));
  };
}

