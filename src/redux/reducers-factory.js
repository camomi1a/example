import convertServerError from '../validation/messagesConverter';

export default function (state, action, config) {
  // console.log(config.stateName, action, action[config.stateName]);
  const { ACTION_STARTED, ACTION_FINISHED, ACTION_SERVER_ERROR, ACTION_SYSTEM_ERROR } = config.const;

  switch (action.type) {
    case ACTION_STARTED:
      return { ...state, error: {}, success: false };
    case ACTION_FINISHED :
      // console.log('---reducer factory', action, config);
      return { ...state, data: action.data, success: true }; // нельзя вернуть просто action.data потому что redux
      // может преобразовать
      // массив к объекту

    case ACTION_SERVER_ERROR: {
      const error = { form: 'Ошибка сервера, обратитесь в поддержку' };

      return { ...state, error, success: false };
    }

    case ACTION_SYSTEM_ERROR: {
      const error = convertServerError(action.data.error || {}, action.data.field);

      return { ...state, error, success: false };
    }
    default:
      return state;
  }
}
